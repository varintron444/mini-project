import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/PopularTourist',
      name: 'PopularTourist',
      component: () => import('../views/PopularTourist.vue')
    },
    {
      path: '/east',
      name: 'east',
      component:() => import('../views/East.vue')
    },
    {
      path: '/north',
      name: 'north',
      component:() => import('../views/North.vue')
    },
    {
      path: '/south',
      name: 'south',
      component:() => import('../views/South.vue')
    },
    {
      path: '/west',
      name: 'west',
      component:() => import('../views/West.vue')
    },
  ]
})

export default router
